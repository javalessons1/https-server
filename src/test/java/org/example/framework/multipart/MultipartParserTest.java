package org.example.framework.multipart;

import lombok.extern.slf4j.Slf4j;
import org.example.framework.http.Request;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.*;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Slf4j
public class MultipartParserTest {

    @Test
    void shouldParseRepeatName() throws IOException {
        final Request request = new Request();
        final String body = readFile("src/test/testRequests/request2.txt");
        request.setContentType("multipart/form-data");
        List<String> expected = new ArrayList<>();
        expected.add("phone");
        expected.add("amount");
        expected.add("color");
        expected.add("emoji");
        final Map<String, List<Part>> part = MultipartParser.parseBody(body, request);
        request.setMultipartParts(part);
        final Map<String, List<Part>> actual = request.getMultipartParts();
        final List<Part> actualParts = actual.get("phone");
        assertEquals(expected.size(), actual.size());
        assertEquals(2, actualParts.size());
    }

    @Test
    void shouldParseNoName() throws IOException {
        final Request request = new Request();
        final String body = readFile("src/test/testRequests/request3.txt");
        request.setContentType("multipart/form-data");
        final String expected = "emoji";
        final Map<String, List<Part>> part = MultipartParser.parseBody(body, request);
        final String content = part.get(expected).get(0).getName();
        assertEquals(expected, content);
    }

    @Test
    void shouldNotParse() throws IOException {
        final Request request = new Request();
        final String body = readFile("src/test/testRequests/request3.txt");
        request.setContentType("application/x-www-form-urlencoded");
        Map<String, List<Part>> expected = new LinkedHashMap<>();
        final Map<String, List<Part>> actual = MultipartParser.parseBody(body, request);
        assertEquals(expected, actual);
    }

    @Test
    void shouldNotParseNoType() throws IOException {
        final Request request = new Request();
        final String body = readFile("src/test/testRequests/request3.txt");
        assertThrows(MultipartParseException.class, () -> {
            MultipartParser.parseBody(body, request);
        });
    }

    @Test
    void shouldNotParseNoBody() {
        final Request request = new Request();
        assertThrows(MultipartParseException.class, () -> {
            MultipartParser.parseBody(null, request);
        });
    }

    public static String readFile(String filename) throws IOException {
        final File f = new File(filename);
        final BufferedReader fin = new BufferedReader(new FileReader(f));
        String line;
        String res = "";
        while (null != (line = fin.readLine())) {
            res += line + "\r\n";
        }
        return res;
    }

}
