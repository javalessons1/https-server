package org.example.framework.http;

import org.example.framework.form.FormParser;
import org.example.framework.multipart.*;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RequestTest {
    @Test
    void shouldParseForm() throws UnsupportedEncodingException {
        Request request = new Request();
        final String formParams = "text=java&type=ebook&type=course";
        request.setContentType("application/x-www-form-urlencoded");
        Map<String, List<String>> expected = new LinkedHashMap<>();
        List<String> text = new ArrayList<>();
        List<String> type = new ArrayList<>();
        text.add("java");
        type.add("ebook");
        type.add("course");
        expected.put("text", text);
        expected.put("type", type);
        final Map<String, List<String>> formData = FormParser.parseForm(formParams, request);
        request.setFormData(formData);
        final Map<String, List<String>> actual = request.getAllFormParams();

        assertEquals(Optional.empty(), request.getFormParam("color"));
        assertEquals(text, request.getFormParams("text"));
        assertEquals(expected.get("type"), actual.get("type"));
    }

    @Test
    void shouldParseMultiple() throws IOException {
        final Request request = new Request();
        Map<String, List<Part>> expected = new LinkedHashMap<>();
        final String body = MultipartParserTest.readFile("src/test/testRequests/request2.txt");
        final byte[] img = MultipartParserTest.readFile("src/test/testRequests/img.txt").getBytes();
        request.setContentType("multipart/form-data");
        List<Part> expectedList = new ArrayList<>();
        List<Part> expectedList1 = new ArrayList<>();
        Map<String, String> phoneHeaders = new LinkedHashMap<>();
        Map<String, String> emojiHeaders = new LinkedHashMap<>();
        phoneHeaders.put("Content-Disposition", "form-data");
        emojiHeaders.put("Content-Disposition", "form-data");
        emojiHeaders.put("Content-Type", "image/png");
        FormFieldPart firstPart = new FormFieldPart("phone", "+7951-xxx-xxxx", phoneHeaders);
        FormFieldPart secondPart = new FormFieldPart("phone", "+79xx-xxx-xxxx", phoneHeaders);
        FilePart thirdPart = new FilePart("emoji", "img.png", img, emojiHeaders);
        expectedList.add(firstPart);
        expectedList.add(secondPart);
        expectedList1.add(thirdPart);
        expected.put("phone", expectedList);
        expected.put("emoji", expectedList1);
        final Map<String, List<Part>> part = MultipartParser.parseBody(body, request);
        request.setMultipartParts(part);
        assertEquals(Optional.empty(), request.getFormParam("query"));
        assertEquals(expected.get("phone").get(0).getHeaders(), request.getMultipartFormFieldPart("phone").get().getHeaders());
        assertEquals(expected.get("emoji").get(0).getHeaders(), request.getMultipartFormFieldPart("emoji").get().getHeaders());
    }
}
