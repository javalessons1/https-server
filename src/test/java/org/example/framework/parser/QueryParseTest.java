package org.example.framework.parser;

import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class QueryParseTest {
    @Test
    void shouldParse() {
        final String body = "text=java&type=ebook&type=course";
        List<String> expectedList = new ArrayList<>();
        expectedList.add("ebook");
        expectedList.add("course");
        final Map<String, List<String>> part = QueryParse.parse(body);
        final List<String> actualList = part.get("type");
        assertEquals(expectedList, actualList);
    }

    @Test
    void shouldNotParseQuery() {
        String body = "POST http://localhost:9999/transfers";
        Map<String, List<String>> expected = new LinkedHashMap<>();
        Map<String, List<String>> actual = QueryParse.parse(body);
        assertEquals(expected, actual);
    }

    @Test
    void shouldNotParseNoBody(){
        assertThrows(QueryParseException.class, () -> {
            QueryParse.parse(null);
        });
    }
}
