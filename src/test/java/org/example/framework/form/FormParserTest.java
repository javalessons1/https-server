package org.example.framework.form;

import org.example.framework.http.Request;
import org.junit.jupiter.api.Test;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FormParserTest {

    @Test
    void shouldParse() throws IOException {
        final Request request = new Request();
        final String body = "phone=%2B79xx-xxx-xxxx&amount=1009&color=%23000000&emoji=1.jpg";
        request.setContentType("application/x-www-form-urlencoded");
        List<String> expectedList = new ArrayList<>();
        expectedList.add("+79xx-xxx-xxxx");
        final Map<String, List<String>> part= FormParser.parseForm(body, request);
        final List<String> actualList = part.get("phone");
        assertEquals(expectedList, actualList);
    }

    @Test
    void shouldParseRepeat() throws IOException {
        final String body = "phone=%2B79xx-xxx-xxxx&phone=%2B767x-xxx-xxxx&amount=1009&color=%23000000&emoji=1.jpg";
        final Request request = new Request();
        request.setContentType("application/x-www-form-urlencoded");
        List<String> expectedList = new ArrayList<>();
        expectedList.add("+79xx-xxx-xxxx");
        expectedList.add("+767x-xxx-xxxx");
        final Map<String, List<String>> part= FormParser.parseForm(body, request);
        final List<String> actualList = part.get("phone");
        assertEquals(expectedList, actualList);
    }

    @Test
    void shouldParseCorrectBody() throws IOException {
        final Request request = new Request();
        final String body = "phone=%2B79xx-xxx-xxxx&phone=%2B767x-xxx-xxxx&amount=1009&color=%23000000&emoji=1.jpg";
        request.setContentType("multipart/form-data");
        Map<String, List<String>> expected = new LinkedHashMap<>();
        final Map<String, List<String>> actual = FormParser.parseForm(body, request);
        assertEquals(expected, actual);
    }
    @Test
    void shouldNotParseNoBody(){
        final Request request = new Request();
        assertThrows(FormParseException.class, () -> {
            FormParser.parseForm(null, request);
        });
    }
    @Test
    void shouldNotParseNoType(){
        final Request request = new Request();
        final String body = "phone=%2B79xx-xxx-xxxx&phone=%2B767x-xxx-xxxx&amount=1009&color=%23000000&emoji=1.jpg";
        assertThrows(FormParseException.class, () -> {
            FormParser.parseForm(body, request);
        });
    }
    @Test
    void shouldNotParseIncorrect() throws IOException {
        final String body = "phone=%2B79xx-xxx-xxxx&phone=&amount=1009&color=%23000000&emoji=";
        final Request request = new Request();
        request.setContentType("application/x-www-form-urlencoded");
        List<String> expected1 = new ArrayList<>();
        expected1.add("+79xx-xxx-xxxx");
        List<String> expected2 = new ArrayList<>();
        final List<String> actual1 = FormParser.parseForm(body, request).get("phone");
        final List<String> actual2 = FormParser.parseForm(body, request).get("emoji");
        assertEquals(expected1, actual1);
        assertEquals(expected2, actual2);
    }
}
