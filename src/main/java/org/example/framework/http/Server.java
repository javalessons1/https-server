package org.example.framework.http;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Singular;
import lombok.extern.slf4j.Slf4j;
import org.example.framework.exception.*;
import org.example.framework.handler.Handler;
import org.example.framework.middleware.Middleware;
import javax.net.ServerSocketFactory;
import javax.net.ssl.*;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Builder
@RequiredArgsConstructor
public class Server {
    private static final int MAX_REQUEST_LINE_AND_HEADERS_SIZE = 4096;

    @Singular
    private final List<Middleware> middlewares;
    @Singular
    private final Map<Pattern, Map<HttpMethods, Handler>> routes;
    @Builder.Default
    private final Handler notFoundHandler = Handler::notFoundHandler;
    @Builder.Default
    private final Handler methodNotAllowed = Handler::methodNotAllowedHandler;
    @Builder.Default
    private final Handler internalServerErrorHandler = Handler::internalServerError;

    public void serveHTTPS(final int port) throws IOException {
        final ServerSocketFactory socketFactory = SSLServerSocketFactory.getDefault();
        try (
                final ServerSocket serverSocket = socketFactory.createServerSocket(port);
        ) {
            final SSLServerSocket sslServerSocket = (SSLServerSocket) serverSocket;
            sslServerSocket.setEnabledProtocols(new String[]{"TLSv1.2"});
            sslServerSocket.setWantClientAuth(true);
            log.info("server listen on {}:{} port", serverSocket.getLocalSocketAddress(), port);
            while (true) {
                try (
                        final SSLSocket socket = (SSLSocket) serverSocket.accept();
                        final InputStream in = new BufferedInputStream(socket.getInputStream());
                        final OutputStream out = socket.getOutputStream();
                ) {
                    try {
                        final Request request = new Request();
                        log.debug("client connected: {}:{}", socket.getInetAddress(), socket.getPort());
                        for (final Middleware middleware : middlewares) {
                            middleware.handle(socket, request);
                        }
                        final byte[] buffer = new byte[MAX_REQUEST_LINE_AND_HEADERS_SIZE];
                        if (!in.markSupported()) {
                            throw new MarkNotSupportedException();
                        }
                        in.mark(MAX_REQUEST_LINE_AND_HEADERS_SIZE);
                        in.read(buffer);
                        RequestParser.parseRequest(request, in, buffer);

                        Map<HttpMethods, Handler> methodToHandlers = null;
                        for (final Map.Entry<Pattern, Map<HttpMethods, Handler>> entry : routes.entrySet()) {
                            final Matcher matcher = entry.getKey().matcher(request.getPath());
                            if (!matcher.matches()) {
                                continue;
                            }
                            request.setPathMatcher(matcher);
                            methodToHandlers = entry.getValue();
                        }
                        try {
                            if (methodToHandlers == null) {
                                throw new MethodNotAllowedException(request.getMethod());
                            }
                            final HttpMethods hh = HttpMethods.valueOf(request.getMethod());
                            final Handler handler = methodToHandlers.get(hh);
                            if (handler == null) {
                                throw new ResourceNotFoundException(request.getPath());
                            }
                            handler.handle(request, out);
                        } catch (MethodNotAllowedException e) {
                            log.error("request method not allowed", e);
                            methodNotAllowed.handle(request, out);
                        } catch (ResourceNotFoundException e) {
                            log.error("can't found request", e);
                            notFoundHandler.handle(request, out);
                        } catch (Exception e) {
                            log.error("can't handle request", e);
                            internalServerErrorHandler.handle(request, out);
                        }
                    } catch (Exception e) {
                        log.error("can't handle request", e);
                        internalServerErrorHandler.handle(new Request(), out);
                    }
                } catch (Exception e) {
                    log.error("some error", e);
                }
            }
        }
    }
}
