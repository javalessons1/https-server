package org.example.framework.http;

import lombok.AllArgsConstructor;
import lombok.Data;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import java.security.Principal;
import java.util.LinkedHashMap;
import org.example.framework.multipart.Part;
import org.example.framework.multipart.FormFieldPart;
import org.example.framework.parser.QueryParseException;

@Slf4j
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Request {
    private String path;
    private byte[] body;
    private String query;
    private String method;
    private String httpVersion;
    private int contentLength;
    private Principal principal;
    private String contentType;
    private Matcher pathMatcher;
    private Map<String, String> headers = new LinkedHashMap<>();
    private Map<String, List<String>> formData = new LinkedHashMap<>();
    private Map<String, List<String>> queryParams = new LinkedHashMap<>();
    private Map<String, List<Part>> multipartParts = new LinkedHashMap<>();

    public String getPathGroup(final String name) {
        return pathMatcher.group(name);
    }

    public String getPathGroup(final int index) {
        return pathMatcher.group(index);
    }

    public Optional<String> getQueryParam(final String name) {
        return getParams(name, queryParams);
    }

    public List<String> getQueryParams(final String name) {
        List<String> params = new ArrayList<>();
        try {
            params = queryParams.get(name);
            return params;
        } catch (Exception e) {
            return params;
        }
    }

    public Map<String, List<String>> getAllQueryParams() {
        return queryParams;
    }

    public Optional<String> getFormParam(final String name) {
        return getParams(name, formData);
    }


    public List<String> getFormParams(final String name) {
        try {
            final List<String> params = formData.get(name);
            return params;
        } catch (Exception e) {
            throw new QueryParseException("can't parse!");
        }
    }

    public Map<String, List<String>> getAllFormParams() {
        return formData;
    }

    public Optional<Part> getMultipartPart(final String name) {
        final Optional<List<Part>> parts = Optional.of(multipartParts.get(name));
        final Optional<Part> part = Optional.ofNullable(parts.get().get(0));
        return part;
    }

    public List<FormFieldPart> getMultipartParts(final String name) {
        List<FormFieldPart> parts = new ArrayList<>();
        parts.add((FormFieldPart) multipartParts.get(name));
        return parts;
    }

    public Map<String, List<Part>> getAllMultipartParts() {
        return multipartParts;
    }

    public Optional<Part> getMultipartFormFieldPart(final String name) {
        final Optional<Part> part = Optional.of(multipartParts.get(name).get(0));
        return part;
    }

    public Optional<Part> getMultipartFilePart(final String name) {
        final Optional<Part> part = Optional.of(multipartParts.get(name).get(0));
        return part;
    }
    private Optional<String> getParams(String name, Map<String, List<String>> formData) {
        try {
            final Optional<List<String>> OptionalParamList = Optional.ofNullable(formData.get(name));
            final Optional<String> firstParam = Optional.ofNullable(OptionalParamList.get().get(0));
            return firstParam;
        } catch (Exception e) {
            return Optional.empty();
        }
    }
}
