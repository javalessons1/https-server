package org.example.framework.auth;

import lombok.RequiredArgsConstructor;

import java.security.Principal;

@RequiredArgsConstructor
public class LoginPrincipal implements Principal {
    private final String login;

    @Override
    public String getName() {
        return login;
    }
}
