package org.example.framework.exception;

public class InvalidRequestStructureException extends RuntimeException {
    public InvalidRequestStructureException() {
    }

    public InvalidRequestStructureException(String message) {
        super(message);
    }

    public InvalidRequestStructureException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidRequestStructureException(Throwable cause) {
        super(cause);
    }

    public InvalidRequestStructureException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
