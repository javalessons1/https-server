package org.example.framework.exception;

public class InvalidHeaderLineStructureException extends InvalidRequestStructureException {
    public InvalidHeaderLineStructureException(String message) {
        super(message);
    }
}
