package org.example.framework.multipart;

import java.util.Map;

public interface Part {
    String getName();

    Map<String, String> getHeaders();
}
