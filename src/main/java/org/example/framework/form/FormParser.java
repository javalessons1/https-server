package org.example.framework.form;

import lombok.extern.slf4j.Slf4j;
import org.example.framework.http.Request;
import org.example.framework.parser.QueryParse;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class FormParser {

    public static Map<String, List<String>> parseForm(final String body, Request request) throws UnsupportedEncodingException {
        final Map<String, List<String>> queryParams = new LinkedHashMap<>();
        try {
            if (request.getContentType().equalsIgnoreCase("application/x-www-form-urlencoded")) {
                final String strBody = URLDecoder.decode(body, StandardCharsets.UTF_8.name());
                final String[] KeyValue = strBody.split("&");
                for (final String pair : KeyValue) {
                    List<String> tempList = new ArrayList<>();
                    final String[] values = pair.split("=");
                    if (values.length > 1) {
                        QueryParse.addParams(queryParams, tempList, values);
                    } else {
                        if (queryParams.containsKey(values[0])) {
                            tempList = queryParams.get(values[0]);
                            queryParams.put(values[0], tempList);
                        } else {
                            queryParams.put(values[0], tempList);
                        }
                    }
                }
                log.debug("Form successfully parsed!");
                return queryParams;
            }
            log.debug("Nothing to parse!");
            return queryParams;
        } catch (Exception e) {
            throw new FormParseException("Can't parse!");
        }
    }
}
