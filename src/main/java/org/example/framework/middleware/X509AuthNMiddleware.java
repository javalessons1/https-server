package org.example.framework.middleware;

import lombok.extern.slf4j.Slf4j;
import org.example.framework.auth.CertificatePrincipal;
import org.example.framework.http.Request;

import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import java.net.Socket;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class X509AuthNMiddleware implements Middleware {
    @Override
    public void handle(final Socket socket, final Request request) {
        if (request.getPrincipal() != null) {
            return;
        }
        try {
            final SSLSession session = ((SSLSocket) socket).getSession();
            final Pattern cn = Pattern.compile("CN=(.*?)(?:,|$)", Pattern.CASE_INSENSITIVE); // from spring security
            final Matcher matcher = cn.matcher(session.getPeerPrincipal().getName());
            if (!matcher.find()) {
                return;
            }
            final String name = matcher.group(1);
            final CertificatePrincipal principal = new CertificatePrincipal(name);
            log.debug("username: {}", name);
            request.setPrincipal(principal);
        } catch (SSLPeerUnverifiedException e) {
            log.error("unverified", e);
        }
    }
}
