package org.example.framework.parser;

import lombok.extern.slf4j.Slf4j;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class QueryParse {
    public static Map<String, List<String>> parse(final String query) {
        Map<String, List<String>> queryParams = new LinkedHashMap<>();
        try {
            final String[] KeyValue = query.split("&");
            for (final String pair : KeyValue) {
                List<String> tempList = new ArrayList<>();
                final String[] values = pair.split("=");
                if (values.length > 1) {
                    addParams(queryParams, tempList, values);
                }
            }
            log.debug("Query successfully parsed!");
            return queryParams;
        }
        catch (Exception e){
            log.debug("Can't parse! {}", e.getMessage());
            throw new QueryParseException();
        }
    }

    public static void addParams(Map<String, List<String>> queryParams, List<String> tempList, final String[] values) {
        if (queryParams.containsKey(values[0])) {
            tempList = queryParams.get(values[0]);
            tempList.add(values[1]);
            queryParams.put(values[0], tempList);
        } else {
            tempList.add(values[1]);
            queryParams.put(values[0], tempList);
        }
    }
}
