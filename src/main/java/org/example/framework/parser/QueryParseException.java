package org.example.framework.parser;

import org.example.framework.exception.InvalidRequestStructureException;

public class QueryParseException extends InvalidRequestStructureException {
    public QueryParseException() {
    }

    public QueryParseException(String message) {
        super(message);
    }
}
